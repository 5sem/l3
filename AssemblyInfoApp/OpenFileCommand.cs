﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using AssemblyInfoLib;
using Microsoft.Win32;
using AssemblyInfo = AssemblyInfoLib.AssemblyInfo;
using ClassInfo = AssemblyInfoLib.ClassInfo;
using NamespaceInfo = AssemblyInfoLib.NamespaceInfo;

namespace AssemblyInfoApp
{
    public class OpenFileCommand : ICommand
    {
        private readonly ViewModel _model;
        public OpenFileCommand(ViewModel model)
        {
            _model = model;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                CollectionMembers.AssemblyInfo viewModelAssemblyInfo = new CollectionMembers.AssemblyInfo();
                string path = dialog.FileName;
                AssemblyInfo info;
                try
                {
                    info = AssemblyInfoResolver.GetAssemblyInfo(path);
                }
                catch (NotAnAssemblyException)
                {
                    MessageBox.Show("Selected file isn't an assembly");
                    return;
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("Specified file not found");
                    return;
                }
                catch (MscorlibMismatchException)
                {
                    MessageBox.Show("Can't load assembly refering to different version of mscorlib");
                    return;
                }
                finally
                {
                    _model.AssemblyInfos = viewModelAssemblyInfo;
                }
                _model.WindowTitle = info.AssemblyName;
                viewModelAssemblyInfo.AssemblyName = info.AssemblyName;
                viewModelAssemblyInfo.AssemblyPath = info.AssemblyPath;
                var namespaceInfos = viewModelAssemblyInfo.NamespaceInfos;
                foreach (KeyValuePair<string, NamespaceInfo> namespaceInfo in info.NamespaceInfos)
                {
                    CollectionMembers.NamespaceInfo viewModelNamespaceInfo = new CollectionMembers.NamespaceInfo
                    {
                        NamespaceName = namespaceInfo.Key
                    };
                    var classInfos = viewModelNamespaceInfo.ClassInfos;
                    foreach (KeyValuePair<string, ClassInfo> classInfo in namespaceInfo.Value.ClassInfos)
                    {
                        CollectionMembers.ClassInfo viewModelClassInfo = new CollectionMembers.ClassInfo
                        {
                            ClassName = classInfo.Key
                        };
                        var memberInfos = viewModelClassInfo.MemberInfos;
                        foreach (AssemblyInfoLib.MemberInfo memberInfo in classInfo.Value.MemberInfos)
                        {
                            memberInfos.Add(memberInfo);
                        }
                        classInfos.Add(viewModelClassInfo);
                    }
                    namespaceInfos.Add(viewModelNamespaceInfo);
                }
                _model.AssemblyInfos = viewModelAssemblyInfo;
            }
        }
    }
}
