﻿using System.Collections.ObjectModel;
using AssemblyInfoLib;

namespace AssemblyInfoApp.CollectionMembers
{
    public class ClassInfo
    {
        public string ClassName { get; set; }
        public ObservableCollection<MemberInfo> MemberInfos { get; set; } = new ObservableCollection<MemberInfo>();
    }
}
