﻿using System.Collections.ObjectModel;

namespace AssemblyInfoApp.CollectionMembers
{
    public class AssemblyInfo
    {
        public string AssemblyName { get; set; }
        public string AssemblyPath { get; set; }
        public ObservableCollection<NamespaceInfo> NamespaceInfos { get; set; } = new ObservableCollection<NamespaceInfo>();
    }
}
