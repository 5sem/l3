﻿using System.Collections.ObjectModel;

namespace AssemblyInfoApp.CollectionMembers
{
    public class NamespaceInfo
    {
        public string NamespaceName { get; set; }
        public ObservableCollection<ClassInfo> ClassInfos { get; set; } = new ObservableCollection<ClassInfo>();
    }
}
