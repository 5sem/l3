﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using AssemblyInfoApp.CollectionMembers;

namespace AssemblyInfoApp
{
    public partial class ViewModel : INotifyPropertyChanged
    {
        private const string AppName = "Assembly explorer";
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand OpenFileCommand { get; }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            propertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _windowTitle;
        public string WindowTitle
        {
            get => _windowTitle; set
            {
                _windowTitle = AppName + ((value?.Length > 0)? ": " + value : "");
                OnPropertyChanged();
            }
        }

        private AssemblyInfo _assemblyInfo;

        public ViewModel()
        {
            _windowTitle = AppName;
            OpenFileCommand = new OpenFileCommand(this);
        }

        public AssemblyInfo AssemblyInfos
        {
            get => _assemblyInfo; set
            {
                _assemblyInfo = value;
                OnPropertyChanged();
            }
        }
    }
}
