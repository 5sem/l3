﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AssemblyInfoLib;
using System.IO;
using FileNotFoundException = AssemblyInfoLib.FileNotFoundException;

namespace AssemblyLibUnitTests
{
    [TestClass]
    public class UnitTests
    {
        private const int AssebmlyInfoClassesCount = 9;
        private const int NamespaceCount = 2;
        private const string NamespaceName = "AssemblyInfoLib";
        private const string ClassName = "AssemblyInfo";

        private static AssemblyInfo _info;
        private static AssemblyInfo GetAssemblyInfo()
        {
            const string AssemblyPath = "AssemblyInfoLib.dll";
            if (_info == null)
            {
                _info = AssemblyInfoResolver.GetAssemblyInfo(AssemblyPath);
            }
            return _info;
        }
        [TestMethod]
        public void TestFileNotExist()
        {
            Assert.ThrowsException<FileNotFoundException>(
                () => AssemblyInfoResolver.GetAssemblyInfo("Wrong file name"));
        }

        [TestMethod]
        public void TestFileIsntAnAssembly()
        {
            const string FileName = "File.txt";
            using (StreamWriter streamWriter = new StreamWriter(new FileStream(FileName, FileMode.OpenOrCreate)))
            {
                streamWriter.WriteLine("Hello");
            }
            Assert.ThrowsException<NotAnAssemblyException>(
                () => AssemblyInfoResolver.GetAssemblyInfo(FileName));
        }

        [TestMethod]
        public void TestLoadAssembly()
        {
            AssemblyInfo info = GetAssemblyInfo();
            Assert.IsTrue(info != null, "Assebmly info = null");
        }

        [TestMethod]
        public void TestNamespaceName()
        {
            AssemblyInfo info = GetAssemblyInfo();
            Assert.IsNotNull(info, "Assembly info is null");
            Assert.IsNotNull(info.NamespaceInfos, "Namespace infos is null");
            Assert.AreEqual(info.NamespaceInfos.Count, NamespaceCount, "Invalid number of namespaces");
            var keys = info.NamespaceInfos.Keys.GetEnumerator();
            keys.MoveNext();

            Assert.AreEqual(keys.Current, NamespaceName, "Namespace name is invalid");
        }

        [TestMethod]
        public void TestClassName()
        {
            AssemblyInfo info = GetAssemblyInfo();
            Assert.IsTrue(info.NamespaceInfos[NamespaceName].ClassInfos.ContainsKey(ClassName));
        }

        [TestMethod]
        public void TestClassesCount()
        {
            AssemblyInfo info = GetAssemblyInfo();
            Assert.AreEqual(info.NamespaceInfos[NamespaceName].ClassInfos.Count, AssebmlyInfoClassesCount);
        }
    }
}
