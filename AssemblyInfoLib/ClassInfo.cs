﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyInfoLib
{
    [Serializable]
    public class ClassInfo
    {
        private readonly IList<MemberInfo> _memberInfos;
        public IList<MemberInfo> MemberInfos { get => _memberInfos; }

        public ClassInfo()
        {
            _memberInfos = new List<MemberInfo>();
        }

        public void AddMemberInfo(MemberInfo memberInfo)
        {
            _memberInfos.Add(memberInfo);
        }
    }
}
