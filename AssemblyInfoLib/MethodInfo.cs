﻿using System;

namespace AssemblyInfoLib
{
    [Serializable]
    public class MethodInfo : MemberInfo
    {
        public bool ExtensionMethod { get; set; }
        public string ReturnType { get; set; }
        public string ParametersInfo { get; set; }
    }
}
