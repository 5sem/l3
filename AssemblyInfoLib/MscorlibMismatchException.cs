﻿using System;
using System.Runtime.Serialization;

namespace AssemblyInfoLib
{
    [Serializable]
    public class MscorlibMismatchException : Exception
    {
        public MscorlibMismatchException() : base() {}

        public MscorlibMismatchException(string message, Exception e) : base(message, e) {}

        protected MscorlibMismatchException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
