﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyInfoLib
{
    [Serializable]
    public class NamespaceInfo
    {
        private readonly Dictionary<string, ClassInfo> _classInfos;
        public Dictionary<string, ClassInfo> ClassInfos { get => _classInfos; }
        public ClassInfo AddOrGetClassInfo(string className)
        {
            if (!_classInfos.ContainsKey(className))
            {
                _classInfos.Add(className, new ClassInfo());
            }
            return _classInfos[className];  
        }

        public NamespaceInfo()
        {
            _classInfos = new Dictionary<string, ClassInfo>();
        }
    }
}
