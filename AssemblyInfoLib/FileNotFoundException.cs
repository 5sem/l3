﻿using System;
using System.Runtime.Serialization;

namespace AssemblyInfoLib
{
    [Serializable]
    public class FileNotFoundException : Exception
    {
        public FileNotFoundException() : base() { }
        public FileNotFoundException(string fileName, Exception e) : base("File not found: " + fileName, e) { }

        protected FileNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
