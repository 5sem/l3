﻿using System;
using System.Runtime.Serialization;

namespace AssemblyInfoLib
{
    [Serializable]
    public class NotAnAssemblyException : Exception
    {
        public NotAnAssemblyException() : base() { }

        public NotAnAssemblyException(string fileName, Exception e) : base("Not an assembly: " + fileName, e) { }

        protected NotAnAssemblyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
