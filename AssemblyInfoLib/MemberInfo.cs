﻿using System;

namespace AssemblyInfoLib
{
    [Serializable]
    public abstract class MemberInfo
    {
        public string MemberName { get; set; }
        public bool IsStatic { get; set; }
    }
}
