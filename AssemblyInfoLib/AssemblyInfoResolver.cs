﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security;
using AssemblyInfoLib.Generators;

namespace AssemblyInfoLib
{
    public class AssemblyInfoResolver : MarshalByRefObject
    {
        private static readonly Dictionary<MemberTypes, IMemberInfoGenerator> _memberGenerators;
        static AssemblyInfoResolver()
        {
            _memberGenerators = new Dictionary<MemberTypes, IMemberInfoGenerator>
            {
                { MemberTypes.Field, new FieldInfoGenerator() },
                { MemberTypes.Property, new PropertyInfoGenerator() },
                { MemberTypes.Method, new MethodInfoGenerator() }
            };
        }
        private static Assembly ResolveEventHandler(Object sender, ResolveEventArgs args)
        {
            Assembly result;
            AssemblyName assemblyName = new AssemblyName(args.Name);
            string folderPath = Path.GetDirectoryName(args.RequestingAssembly.Location);
            string expectedAssemblyPath = Path.Combine(folderPath, assemblyName.Name + ".dll");
            try
            {
                result = Assembly.ReflectionOnlyLoad(assemblyName.FullName);
            }
            catch (System.IO.FileNotFoundException)
            {
                result = Assembly.ReflectionOnlyLoadFrom(expectedAssemblyPath);
            }
            return result;
        }

        internal AssemblyInfo InternalGetAssemblyInfo(string assemblyPath)
        {
            Assembly assembly;
            AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += ResolveEventHandler;

            try
            {
                assembly = Assembly.ReflectionOnlyLoadFrom(assemblyPath);
            }
            catch (System.IO.FileNotFoundException e)
            {
                throw new AssemblyInfoLib.FileNotFoundException(assemblyPath, e);
            }
            catch (BadImageFormatException e)
            {
                throw new NotAnAssemblyException(assemblyPath, e);
            }
            var assemblyName = assembly.FullName;
            var assemblyFullPath = assembly.CodeBase;
            AssemblyInfo result = new AssemblyInfo(assemblyName, assemblyFullPath);
            Type[] types = null;
            try
            {
                types = assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                throw new MscorlibMismatchException(assemblyPath, e);
            }

            Dictionary<string, Dictionary<string, List<MethodInfo>>> extensionMethods = new Dictionary<string, Dictionary<string, List<MethodInfo>>>();
            foreach (Type type in types)
            {
                foreach (System.Reflection.MemberInfo memberInfo in type.GetMembers(
                    BindingFlags.Public
                    | BindingFlags.NonPublic
                    | BindingFlags.Instance
                    | BindingFlags.Static
                    | BindingFlags.DeclaredOnly))
                {
                    string namespaceName = type.Namespace ?? "global";
                    string className = type.Name;
                    if (_memberGenerators.ContainsKey(memberInfo.MemberType))
                    {
                        var viewModeMemberInfo = _memberGenerators[memberInfo.MemberType].GetMemberInfo(memberInfo, ref namespaceName, ref className);
                        var namespaceInfo = result.AddOrGetNamespaceInfo(namespaceName);
                        var classInfo = namespaceInfo.AddOrGetClassInfo(className);
                        classInfo.AddMemberInfo(viewModeMemberInfo);
                    }
                }
            }
            return result;
        }

        public static AssemblyInfo GetAssemblyInfo(string assemblyPath)
        {
            var appName = "assemblyLibLoadingDomain";
            var domainSetup = new AppDomainSetup
            {
                ApplicationName = appName,
                ShadowCopyFiles = "false",
                ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase,
                ConfigurationFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile,
                DynamicBase = AppDomain.CurrentDomain.SetupInformation.DynamicBase,
                LicenseFile = AppDomain.CurrentDomain.SetupInformation.LicenseFile,
                LoaderOptimization = AppDomain.CurrentDomain.SetupInformation.LoaderOptimization,
                PrivateBinPath = AppDomain.CurrentDomain.SetupInformation.PrivateBinPath,
                PrivateBinPathProbe = AppDomain.CurrentDomain.SetupInformation.PrivateBinPathProbe
            };
            AppDomain appDomain = AppDomain.CreateDomain("assemblyLibLoadingDomain", AppDomain.CurrentDomain.Evidence, domainSetup, new PermissionSet(System.Security.Permissions.PermissionState.Unrestricted));

            AssemblyInfo result = null;
            try
            {
                var type = typeof(AssemblyInfoResolver);
                var infoResolver = (AssemblyInfoResolver)appDomain.CreateInstanceAndUnwrap(type.Assembly.FullName, type.FullName);
                result = infoResolver.InternalGetAssemblyInfo(assemblyPath);
            }
            catch (System.IO.FileNotFoundException)
            {
                throw new FileNotFoundException();
            }
            finally
            {
                AppDomain.Unload(appDomain);
            }
            return result;
        }
    }
}
