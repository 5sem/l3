﻿namespace AssemblyInfoLib
{
    [System.Serializable]
    public class FieldInfo : MemberInfo
    {
        public string FieldType { get; set; }
    }
}
