﻿using System;
using System.Collections.Generic;

namespace AssemblyInfoLib
{
    [Serializable]
    public sealed class AssemblyInfo
    {
        // namespace -> (typeName -> members);
        internal Dictionary<string, NamespaceInfo> _namespaceInfos;

        public Dictionary<string, NamespaceInfo> NamespaceInfos { get => _namespaceInfos; }
        public string AssemblyName { get; }
        public string AssemblyPath { get; }

        internal NamespaceInfo AddOrGetNamespaceInfo(string namespaceName)
        {
            if (!_namespaceInfos.ContainsKey(namespaceName))
            {
                _namespaceInfos.Add(namespaceName, new NamespaceInfo());
            }
            return _namespaceInfos[namespaceName];
        }

        internal AssemblyInfo(string assemblyName, string assemblyPath)
        {
            _namespaceInfos = new Dictionary<string, NamespaceInfo>();
            AssemblyName = assemblyName;
            AssemblyPath = assemblyPath;
        }
    }
}
