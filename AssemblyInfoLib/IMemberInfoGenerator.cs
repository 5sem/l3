﻿namespace AssemblyInfoLib
{
    interface IMemberInfoGenerator
    {
       MemberInfo GetMemberInfo(System.Reflection.MemberInfo memberInfo, ref string namespaceName, ref string className);
    }
}
