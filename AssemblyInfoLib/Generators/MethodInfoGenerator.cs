﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace AssemblyInfoLib.Generators
{
    class MethodInfoGenerator : IMemberInfoGenerator
    {
        public MemberInfo GetMemberInfo(System.Reflection.MemberInfo memberInfo, ref string namespaceName, ref string className)
        {
            if (memberInfo?.MemberType == MemberTypes.Method)
            {
                System.Reflection.MethodInfo methodInfo = memberInfo as System.Reflection.MethodInfo;
                MethodInfo viewModelMethodInfo = new MethodInfo
                {
                    ReturnType = methodInfo.ReturnType.Name,
                    MemberName = methodInfo.Name,
                    IsStatic = methodInfo.IsStatic
                };
                
                if (CustomAttributeData.GetCustomAttributes(methodInfo)
                    .Any(data => data.AttributeType == typeof(ExtensionAttribute)))
                {
                    viewModelMethodInfo.ExtensionMethod = true;
                    viewModelMethodInfo.IsStatic = false;
                    Type extensibleType = methodInfo.GetParameters()[0].ParameterType;
                    namespaceName = extensibleType.Namespace;
                    className = extensibleType.Name;
                }

                ParameterInfo[] parameters = methodInfo.GetParameters();
                string parametersInfo = "";
                if (parameters.Length > 0)
                {
                    foreach (ParameterInfo parameter in parameters)
                    {
                        parametersInfo += parameter.ParameterType.Name + " " + parameter.Name + ", ";
                    }
                    parametersInfo = parametersInfo.Substring(0, parametersInfo.Length - 2);
                }
                viewModelMethodInfo.ParametersInfo = parametersInfo;
                return viewModelMethodInfo;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
