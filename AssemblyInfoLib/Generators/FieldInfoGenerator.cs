﻿using System;
using System.Reflection;

namespace AssemblyInfoLib.Generators
{
    class FieldInfoGenerator : IMemberInfoGenerator
    {
        public MemberInfo GetMemberInfo(System.Reflection.MemberInfo memberInfo, ref string namespaceName, ref string className)
        {
            if (memberInfo?.MemberType == MemberTypes.Field)
            {
                System.Reflection.FieldInfo fieldInfo = memberInfo as System.Reflection.FieldInfo;
                FieldInfo viewModelFieldInfo = new FieldInfo
                {
                    FieldType = fieldInfo.FieldType.Name,
                    MemberName = memberInfo.Name,
                    IsStatic = fieldInfo.IsStatic
                };
                return viewModelFieldInfo;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
