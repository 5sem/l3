﻿using System;
using System.Reflection;

namespace AssemblyInfoLib.Generators
{
    class PropertyInfoGenerator : IMemberInfoGenerator
    {
        public MemberInfo GetMemberInfo(System.Reflection.MemberInfo memberInfo, ref string namespaceName, ref string className)
        {
            if (memberInfo?.MemberType == MemberTypes.Property)
            {
                System.Reflection.PropertyInfo property = memberInfo as System.Reflection.PropertyInfo;
                PropertyInfo viewModelPropertyInfo = new PropertyInfo
                {
                    MemberName = property.Name,
                    PropertyType = property.PropertyType.Name,
                    HasGetter = property.CanRead,
                    HasSetter = property.CanWrite,
                    IsStatic = property.CanRead ? property.GetGetMethod(true).IsStatic : property.GetSetMethod(true).IsStatic
                };
                return viewModelPropertyInfo;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
